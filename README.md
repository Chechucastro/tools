# Tools
All the necessary Gulp tools for your web application development.
---
# Getting Started

You need to have Node.js (Node) installed onto your computer before you can install Gulp.

If you do not have Node installed already (I presume you already have it because you normally use GRUNT), you can get it by downloading the package installer from **[Nodes's website.](https://nodejs.org/en/download/)**.

When you're done with installing Node, you can install Gulp by using the following command in the command line:

INSTALLS

- Install gulp globally:

~~~~
npm install gulp -g
~~~~

- Install bower globally:
~~~~
npm install -g bower
~~~~

- Now go to your Gulp folder's root and install all the modules need it (This will install all the dependencies found in your package.json file)

~~~~
npm install
~~~~

- Install Bootstrap with bower : (bower.json)

~~~~
bower install
~~~~

- That's it, you can run gulp:
~~~~
gulp
~~~~

# Available tasks (gulpfile.js)

#### Available commands. What they do:
~~~~
"gulp" :

- BrowserSync : Initialize  server locally.

- Sass : Watch for sass changes, if changes are made, then it converts sass to Css. This task create also inline css maps.

- Js   : Watch for any JavaScript change. This task copy the current js file to its "js" folder.

- Images : Drag and drop your image into the "imgs" folder. This task Optimize only the new added image and copy it to its network folder.
Ex: From C:/swg/swgastu2/imgs/myimage.jpg  copy to -> A:/imgs/myimage.jpg

"gulp copy" :  (this command copy all the generated css, js, images and thymeleaf files to your network location (Please edit your config.json file first to match your desired location))

~~~~
Do not forget to "kill" any running node process before swiching between running commands (Ctlr+C then type "O")

# TODO
- Think about how to work with the thymeleaf files locally.


# Gulp Plugins
http://gulpjs.com/plugins/
