/* =======================================================================
This file is created by Chechu Castro [chechu@digitatis.com]
In order to use this code (or a part of it), please feel free
to contact me.
Website : http://www.digitatis.com
FILE NAME: gulpfile.js - Copyright (C)
======================================================================= */

// GULP
var gulp    = require('gulp'),

// CONFIG FILE
fs 					= require('fs'),
config 			= JSON.parse(fs.readFileSync('./config.json')),

// THE MODULES
browserSync = require('browser-sync').create(),
sass 				= require('gulp-sass'),
stripCssComments = require('gulp-strip-css-comments'),
cleanCSS 		= require('gulp-clean-css'),
bytediff 		= require('gulp-bytediff'),
sourcemaps 	= require('gulp-sourcemaps'),
autoprefixer= require('gulp-autoprefixer'),
concat      = require('gulp-concat'),
watch       = require('gulp-watch'),
plumber     = require('gulp-plumber'),
newer       = require('gulp-newer'),
imagemin 		= require('gulp-imagemin');

// DEBUGGING...
// console.log("Your Drive Letter : " , config.env.copy.drive_letter);
// console.log("Enviromment SASS : " , config.env.dev.sass);
// console.log("Enviromment Thymeleaf : " , config.env.dev.thymeleaf);
// console.log("Destination CSS : ", config.env.dev.css);
// console.log("Project url : ", config.env.dev.url);
// console.log('Copy the generated css file to: ', config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/css');
// console.log('Copy the generated JS file to: ', config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/js');
// console.log('Copy from thymeleaf to your local Project: ', config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/thymeleaf');

// ERROR HANDLER
var onError = function (err) {
	console.log(err);
	this.emit('end');
};

function replaceByLocalCSS() {
		var url = require("url");
		return function (req, res, next) {
			var filename = null;
			var parsed   = url.parse(req.url);
			var match    = parsed.pathname.match(/style\.css(?:.map)?$/);
			if (match) {
				filename = config.env.dev.css + '/' + match[0];
			}
			else if(match = parsed.pathname.match(/[^\/]+.scss$/)) {
				filename = config.env.dev.sass + '/' + match[0];
			}
			if (null !== filename) {
				var contentType = 'text/css; charset=utf-8';
				if (/scss$/.test(filename)) {
					contentType = 'text/scss; charset=utf-8';
				}
				res.setHeader('Content-Type', contentType);
				return res.end(fs.readFileSync(filename));
			}
			next();
		};
	}

// BROWSERSYNC
gulp.task('server', function(){
	return browserSync.init(null, {
		proxy: config.env.dev.url,
		middleware: replaceByLocalCSS()
	});
});

// SASS
gulp.task('sass', function () {
	return gulp.src(config.env.dev.sass)
	.pipe(plumber({errorHandler: onError}))
	.pipe(sourcemaps.init())
	.pipe(sass({
		outputStyle: 'expanded', // nested, compact, expanded, compressed
		includePaths: [''+config.env.dev.bootstrap+'']}))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.env.dev.css))
		.pipe(browserSync.stream());
	});

	// JAVASCRIPT
	gulp.task('js', function () {
		return gulp.src(config.env.dev.js)
		.pipe(plumber({errorHandler: onError}))
		.pipe(gulp.dest(config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/js'))
		.pipe(browserSync.stream());
	});

	// Copy Thymeleaf files from your Network drive to your root project folder
	gulp.task('copy_Thymeleaf_From_REC_To_LOCAL', function() {
		return gulp.src(config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/thymeleaf/**/')
		.pipe(plumber({errorHandler: onError}))
		.pipe(gulp.dest(config.env.dev.thymeleaf));
	});

	// Copy generated CSS + Thymeleaf files
	gulp.task('copy_Css_To_REC', function () {
		// Copy generated Css files to your network drive
		return gulp.src(config.env.dev.css+'/style.css')
		.pipe(plumber({errorHandler: onError}))
		.pipe(bytediff.start())
		.pipe(cleanCSS({
				debug: true,
				compatibility: '*'
			},function(details) {
				console.log('Le Fichier ' + details.name + ' -> [ CSS Original ] -> ' + details.stats.originalSize + ' ko');
				console.log('Le Fichier ' + details.name + ' -> [ CSS Minifié ] -> ' + details.stats.minifiedSize + ' ko');
		}))
		.pipe(bytediff.stop(function(data) {
        var difference = (data.savings > 0) ? ' plus petit' : ' plus gros';
				var porcentage = (((data.startSize - data.endSize) / data.startSize) * 100).toFixed(0);
        return data.fileName + ' -> [ ' + porcentage + '%' + difference+' ]';
    }))
		.pipe(stripCssComments({preserve: false}))
		.pipe(gulp.dest(config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/css'));
	});

	// Copy generated JS file to RECETTE
	gulp.task('copy_JS_To_REC', function () {
		// Copy generated Css files to your network drive
		return gulp.src(config.env.dev.js+'/*')
		.pipe(plumber({errorHandler: onError}))
		.pipe(gulp.dest(config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/js'));
	});

	// Optimize & copy only new Images to your network
	gulp.task('copy_Images_To_REC', function() {
		return gulp.src(config.env.dev.imgs+'/*')
			.pipe(plumber())
			.pipe(newer(config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/imgs/*'))
			.pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
			.pipe(gulp.dest(config.env.copy.drive_letter+'/'+config.env.rec.projectFolder+'/imgs'));
	});

	// WATCHERS -> Sass & Js
	gulp.task('watch', function() {
		gulp.watch(config.env.dev.sass, ['sass']);
		gulp.watch(config.env.dev.js, ['js']);
		gulp.watch(config.env.dev.imgs+'/*', ['copy_Images_To_REC']);
	});

	// Watch for changes
	gulp.task('bs', ['server','watch']);

	// Default task -> "gulp"
	gulp.task('default', ['bs']);

	// Copy generated files -> "gulp copy" (Copy the Css, images and Thymeleaf files to the right folder)
	gulp.task('copy', ['copy_JS_To_REC','copy_Css_To_REC','copy_Thymeleaf_From_REC_To_LOCAL','copy_Images_To_REC']);
